import controlP5.*;
import processing.video.*;

Movie myImage; //////////////////

float gridSize = 20;
float crossSize = 15;
float margin = 20;

ControlP5 cp5;

void setup() {
  size(1280, 720);

  myImage = new Movie(this, "video.mp4"); //////////////////
  myImage.loop(); //////////////////
  
  cp5 = new ControlP5(this);
  cp5.addSlider("gridSize").setPosition(50, 50).setRange(20, 400);
  cp5.addSlider("crossSize").setPosition(50, 70).setRange(5, 350);
  cp5.addSlider("margin").setPosition(50, 90).setRange(5, 350);

  strokeCap(SQUARE);
}

void draw() {
  blendMode(ADD);
  
  if (myImage.available()) {
    myImage.read();
    myImage.loadPixels();
  }

  background(0);
  noFill();

  color colorA = #00AAAA;
  color colorB = #AA1955;
  color colorC = #AA7700;

  if (myImage.pixels != null && myImage.pixels.length > 0) {
    for (float x = margin; x < width - margin; x += gridSize) {
      for (float y = margin; y < height - margin; y += gridSize) {
        float centerX = x + gridSize / 2;
        float centerY = y + gridSize / 2;

        int index = floor(x + y * width);

        color pixelColor = myImage.pixels[index];

        float redChannel = red(pixelColor);
        float redStrokeWeight = map(redChannel, 0, 255, 1, crossSize);

        float greenChannel = green(pixelColor);
        float greenStrokeWeight = map(greenChannel, 0, 255, 1, crossSize);

        float blueChannel = blue(pixelColor);
        float blueStrokeWeight = map(blueChannel, 0, 255, 1, crossSize);

        pushMatrix();
        translate(centerX, centerY);

        stroke(colorA);
        strokeWeight(redStrokeWeight);

        line(-crossSize / 2, -crossSize / 2, crossSize / 2, crossSize / 2);

        stroke(colorB);
        strokeWeight(greenStrokeWeight);

        line(crossSize / 2, -crossSize / 2, -crossSize / 2, crossSize / 2);

        stroke(colorC);
        strokeWeight(blueStrokeWeight);

        line(-crossSize / 2, -crossSize / 2, crossSize / 2, crossSize / 2);
        line(crossSize / 2, -crossSize / 2, -crossSize / 2, crossSize / 2);

        popMatrix();
      }
    }
  }
  
  blendMode(NORMAL);
}
