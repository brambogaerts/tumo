import processing.video.*;

Capture myImage;

void setup() {
  size(1280, 720);

  myImage = new Capture(this, Capture.list()[0]);
  myImage.start();

  strokeCap(SQUARE);
  blendMode(MULTIPLY);
}

void draw() {
  if (myImage.available()) {
    myImage.read();
    myImage.loadPixels();
  }

  background(255);
  noFill();

  int gridSize = 20;
  int crossSize = 15;
  int margin = 20;

  color colorA = #00D5FF;
  color colorB = #FF1EE9;
  color colorC = #FFEF00;

  if (myImage.pixels != null && myImage.pixels.length > 0) {
    for (int x = margin; x < width - margin; x += gridSize) {
      for (int y = margin; y < height - margin; y += gridSize) {
        int centerX = x + gridSize / 2;
        int centerY = y + gridSize / 2;

        int index = x + y * width;

        color pixelColor = myImage.pixels[index];

        float redChannel = red(pixelColor);
        float redStrokeWeight = map(redChannel, 0, 255, 1, crossSize);

        float greenChannel = green(pixelColor);
        float greenStrokeWeight = map(greenChannel, 0, 255, 1, crossSize);

        float blueChannel = blue(pixelColor);
        float blueStrokeWeight = map(blueChannel, 0, 255, 1, crossSize);

        pushMatrix();
        translate(centerX, centerY);

        stroke(colorA);
        strokeWeight(redStrokeWeight);

        line(-crossSize / 2, -crossSize / 2, crossSize / 2, crossSize / 2);

        stroke(colorB);
        strokeWeight(greenStrokeWeight);

        line(crossSize / 2, -crossSize / 2, -crossSize / 2, crossSize / 2);

        stroke(colorC);
        strokeWeight(blueStrokeWeight);

        line(-crossSize / 2, -crossSize / 2, crossSize / 2, crossSize / 2);
        line(crossSize / 2, -crossSize / 2, -crossSize / 2, crossSize / 2);

        popMatrix();
      }
    }
  }
}
