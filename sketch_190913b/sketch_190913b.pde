PImage image;
PImage[] units;

void setup() {
  size(1920, 1080);
  
  image = loadImage("ararat.jpg");
  image.loadPixels();
  
  units = new PImage[9];
  
  for (int i = 0; i < units.length; i++) {
    units[i] = loadImage("series/" + (i + 1) + ".png");
  }
  
  blendMode(ADD);
}

void draw() {
  background(0);
  
  int gridSize = 50;
  
  for(int x = 0; x < width; x += gridSize) {
    for(int y = 0; y < height; y += gridSize) {
      color c = image.pixels[x + y * width];
      
      float redChannel = red(c);
      int unitIndexRedChannel = round(map(redChannel, 0, 255, 0, units.length - 1));
      
      float greenChannel = green(c);
      int unitIndexGreenChannel = round(map(greenChannel, 0, 255, 0, units.length - 1));
      
      float blueChannel = blue(c);
      int unitIndexBlueChannel = round(map(blueChannel, 0, 255, 0, units.length - 1));
      
      tint(255, 0, 0);
      image(units[unitIndexRedChannel], x, y, gridSize, gridSize);
      
      tint(0, 255, 0);
      image(units[unitIndexGreenChannel], x, y, gridSize, gridSize);
      
      tint(0, 0, 255);
      image(units[unitIndexBlueChannel], x, y, gridSize, gridSize);
    }
  }
}
